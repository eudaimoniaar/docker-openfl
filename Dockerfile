FROM haxe:latest

RUN haxelib install openfl 9.1.0

RUN yes n | haxelib run openfl setup

RUN cp `haxelib libpath lime`templates/bin/lime.sh /usr/local/bin/lime
RUN chmod 755 /usr/local/bin/lime
RUN cp `haxelib libpath openfl`assets/templates/bin/openfl.sh /usr/local/bin/openfl
RUN chmod 755 /usr/local/bin/openfl
